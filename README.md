# INSTALLATION OPENSTACK


---------------------------------------------------------------------------------------------------------------
## PREPARATION DE L'ENVIRONNEMENT:
---------------------------------------------------------------------------------------------------------------

### Install puppet-agent:
```bash
sudo rpm -Uvh https://yum.puppet.com/puppet6-release-el-7.noarch.rpm
sudo yum install -y puppet-agent git
```

### Download Git project Workstation:
```bash
cd /opt
git clone https://gitlab.com/phobos-openstack/openstack-standalone.git controller
```

### CONFIGURATION DU MODULE OPENSTACK-STANDALONE:
Vous devez renseigner les informations correspondant à votre réseau local dans le fichier: /opt/controller/data/common.yaml. La plage d'adresse disponible (start-end), votre server DNS, Passerelle (Gateway) et l'adressage CIDR du réseau.

controller::network_start: '192.168.0.2'  
controller::network_end: '192.168.0.254'
controller::network_dns: '192.168.0.1'
controller::network_gateway: '192.168.0.1'
controller::network_range: '192.168.0.0/24'


---------------------------------------------------------------------------------------------------------------
## PREREQUIS:
---------------------------------------------------------------------------------------------------------------

### CARTE RESEAU:
Configurer votre carte réseau Ethernet pour qu'elle pointe sur le bridge externe.
```bash
$ vi /etc/sysconfig/network-scripts/ifcfg-...
DEVICE=...
TYPE=OVSPort
DEVICETYPE=ovs
OVS_BRIDGE=br-ex
ONBOOT=yes
NM_CONTROLLED=no
BOOTPROTO=none
```
*Votre carte Ethernet doit être connecté a un réseau disposant d'un serveur DHCP et DNS.
*(Actuellement, ne fontionne pas avec la carte Wifi) 


### PACKAGE QEMU
Nova requiere QEMU version 2.8.0 pour fonctionner correctement. Ce paquet sera installé lors du déploiement du module "openstack-standalone". Toutfois, supprimer le paquet "qemu-system-x86" pose problème ($ virsh version)). 
```bash
yum remove qemu-system-x86
```

### FIREWALL & SELinux:
Si un pare-feu restrictifs et SELinux sont en en place, vous devrez les configurer pour autoriser le trafic des services OpenStack ou les désactiver.
```bash
$ systemctl stop firewalld
$ setenforce 0
```
* Voir la liste des ports OPENSTACK: https://docs.openstack.org/install-guide/firewalls-default-ports.html



### CINDER SERVER :
Le service de stockage (cinder) fournit des périphériques de stockage de type bloc (disque dur) aux instances openstack (VM). 
Il existe une variété de pilotes pris en charge par Cinder: NAS / SAN, NFS, iSCSI, Ceph... Selon les pilotes utilisés, le service de volume peut s'exécuter sur des nodes controller, compute ou des nodes de stockage autonomes.

Ici, CINDER fournit des volumes logiques au VM en utilisant le pilote LVM. Pour ce faire, vous devez allouer un disque dédié à CINDER:

1/ Avec fdisk créer une partition qui sera utiliser par Cinder

2/ Avec LVM Créez le volume physique (pv) :
```bash
$ lvmdiskscan
$ pvcreate /dev/...
```
* En cas de problème : https://www.thegeekdiary.com/lvm-error-cant-open-devsdx-exclusively-mounted-filesystem/

3/ Avec LVM créez le volume groupe (vg) nommé "cinder-volumes" :
```bash
$ vgcreate cinder-volumes /dev/...
$ vgs
```

Maintenant, CINDER pourrat crée des volumes logiques dans ce groupe de volumes.
Seules les instances peuvent accéder aux volumes de stockage de bloc. 




### SWIFT 
Le service de stockage (SWIFT) fournit des périphériques de stockage de type Object (fichier) à l'environnement Openstack. 
Pour que celui-ci fonctionne correctement, vous devez lui associé un mimimum de deux disque. L'exemple ci-dessous suppose que le server de Stockage SWIFT contient les disques /dev/sdb et /dev/sdc.

Formatez les périphériques /dev/sdb et /dev/sdc tant que XFS:
```bash
$ mkfs.xfs /dev/sdb
$ mkfs.xfs /dev/sdc
```

Créez le répertoire des points de montage des disques:
```bash
$ mkdir -p /mnt/swift/sdb
$ mkdir -p /mnt/swift/sdc
```

Editez le fichier /etc/fstab et ajoutez-y les éléments suivants:
```bash
/dev/sdb /mnt/swift/sdb xfs noatime,nodiratime,nobarrier,logbufs=8 0 2
/dev/sdc /mnt/swift/sdc xfs noatime,nodiratime,nobarrier,logbufs=8 0 2
```

Montez les disques:
```bash
$ mount /mnt/swift/sdb
$ mount /mnt/swift/sdc
```




---------------------------------------------------------------------------------------------------------------
## LANCER L'INSTALLATION
---------------------------------------------------------------------------------------------------------------

### Install Openstack:
```bash
puppet apply --modulepath=/opt -e "include controller" -v
```










---------------------------------------------------------------------------------------------------------------
# CONFIGURER ET UTILISER OPENSTACK



---------------------------------------------------------------------------------------------------------------
## DASHBOARD OPENSTACK (HORIZON)
---------------------------------------------------------------------------------------------------------------
Accédez au tableau de bord à l'aide d'un navigateur Web : 
```bash
http://controller/dashboard
```

login: admin
password: PWDGOP


Voir utilisation du Dashboard Openstack: https://docs.openstack.org/horizon/rocky/user/


---------------------------------------------------------------------------------------------------------------
## CLI OPENSTACK 
---------------------------------------------------------------------------------------------------------------
Pour pouvoir utiliser le CLI Openstack, vous devez sourcer votre configuration admin:
```bash	
source /root/admin-openrc.sh
```

Voir utilisation CLI: https://docs.openstack.org/python-openstackclient/rocky/cli/command-objects/server.html#server-create


---------------------------------------------------------------------------------------------------------------
## UTILISATEUR & PROJET (KEYSTONE) 
---------------------------------------------------------------------------------------------------------------
Le service d'authentification (KEYSTONE) utilise une combinaison de domaines, projets, users, rôles…

Créer du projet "projet1" et vérifier la création: 
```bash		
$ openstack project create --domain default --description "demo Projet" projet1
$ openstack project list
```
     *Pour supprimer le project:
     ```bash
     $ openstack project delete projet1

     ```

Créer un utilisateur "user1", renseigner le mot de passe et vérifier la création:
```bash	
$ openstack user create --domain default --password-prompt user1
$ openstack user list
```
     *Pour supprimer l'utilisateur:
     ```bash
     $ openstack user delete user1

     ```

Donner à l'utilisateur "user1" le rôle "member" pour le projet "projet1": 
```bash
$ openstack role add --project projet1 --user user1 member
```

Utiliser un nouveau Token d'authentification (KEYSTONE) pour l’utilisateur "user1" et vérifier son fonctionnement:
```bash
$ unset OS_AUTH_URL OS_PASSWORD
$ openstack --os-auth-url http://controller:5000/v3 --os-project-domain-name default --os-project-domain-name default --os-project-name projet1 --os-username user1 token issue
```
*Cette commande utilise le mot de passe de l'utilisateur "user1" et n'autorise qu'un accès normal (non administrateur) à l'API KEYSTONE.

Interroger Keystone à avec l'utilisateur "user1":
```bash
openstack --os-auth-url http://controller:5000/v3 --os-project-domain-name default --os-project-domain-name default --os-project-name projet1 --os-username user1 project list

Créer un script d'environnement "user1.sh" pour l'utilisateur "user1" (changer le mot de passe):
```bash
unset OS_SERVICE_TOKEN
export OS_USERNAME=user1
export OS_PASSWORD=user1
export OS_AUTH_URL=http://controller:5000/v3
export PS1='[\u@\h \W(keystone_user1)]\$ '
export OS_PROJECT_NAME=projet1
export OS_USER_DOMAIN_NAME=default
export OS_PROJECT_DOMAIN_NAME=default
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
```

Sourcer vos variables d'environnement et dedemander un nouveau jeton d'authentification pour l'utilisateur "user1" :
```bash
$ source /root/admin-openrc.sh
$ openstack token issue
```

Accédez au tableau de bord à l'aide d'un navigateur Web (login: user1 / password: user1): 
```bash
http://controller/dashboard
```





---------------------------------------------------------------------------------------------------------------
# ADMINISTRER & CONFIGURER OPENSTACK


Sourcer votre configuration admin:
```bash	
source /root/admin-openrc.sh
```


---------------------------------------------------------------------------------------------------------------
## Les Quotas 
---------------------------------------------------------------------------------------------------------------
Des quotas sont disponibles pour limiter le nombre d’object par projet (Load Balancer, Pool, member, network..). 

Vérifer les quotas par défaut:
```bash
$ openstack quota show default
```

Modifier un quota par defaut:
```bash
openstack quota set --backup-gigabytes 100 default
```

Vérifer et modifier un quota associé a un projet:
```bash
$ openstack quota show projet1
$ openstack quota set --backup-gigabytes 10 projet1
```


---------------------------------------------------------------------------------------------------------------
## Les images (GLANCE)
---------------------------------------------------------------------------------------------------------------
GLANCE permet de mettre à disposition de vos utilisateur un ensemble d'images système. 

Télécharger et importer une image système dans Openstack GLANCE :
```bash
$ wget wget http://download.cirros-cloud.net/0.3.5/cirros-0.3.5-x86_64-disk.img
$ openstack image create "cirros" --file cirros-0.3.5-x86_64-disk.img --disk-format qcow2 --container-format bare --public
```
*Vous pouvez importer des image et modifier leurs visibilité sur la plateforme Openstack (private/public). 


Confirmer la bonne importation dans Glance de l'image (par ID ou NOM):
```bash
$ openstack image list
$ openstack image show "ID/IMAGE"
```

Pour supprimer une image :
```bash
$ openstack delete "ID/NAME IMAGE"
```

Pour rendre "public" une image pour la rendre disponible à tous les utilisateurs et tous les projets:
```bash
$ openstack image set --public "ID/NAME IMAGE"
```

Pour rendre "private" une image :
```bash
$ openstack image set --private "ID/NAME IMAGE"
```

Pour trouver plus d'images : https://docs.openstack.org/image-guide/obtain-images.html



---------------------------------------------------------------------------------------------------------------
## Le réseau Provider (NEUTRON)
---------------------------------------------------------------------------------------------------------------
### Réseau externe (Provider) : 
Le réseaux 'Provider' est disponible pour tous les projets. Ce réseau comprend un "Subnet" qui utilise un serveur DHCP externe (sur le réseau provider) qui fournit les adresses IP (Externe) aux instances directement connecté. Ainsi, l'ensemble des instances connecté à se réseau sont accessible depuis l’extérieur.

Créer un réseau PROVIDER et afficher le resultat: 
```bash
$ openstack network create --share --external --provider-physical-network provider --provider-network-type flat provider
$ openstack network list
```
*L'option --external définit le réseau virtuel comme externe. Si vous souhaitez créer un réseau interne, vous pouvez utiliser --internal à la place. (l'option --external" permettra aux routeurs libre-service de l'utiliser pour la connectivité externes). 


Associer un 'SUBNET' du réseau PROVIDER:
```bash
$ openstack subnet create --network provider --allocation-pool start=192.168.122.2,end=192.168.122.254 --dns-nameserver 192.168.122.1 --gateway 192.168.122.1 --subnet-range 192.168.122.0/24 subpro
$ openstack subnet list
```
*Le réseau du PROVIDER utilise 192.168.122.0/24 avec la gateway 192.168.122.1. 
*Le serveur DHCP présent sur le réseau provider attribue à chaque instance openstack une adresse IP allant de 192.168.122.2 à 192.168.122.254 (Cette plage devra être réservé au niveau du DHCP).
*Toutes les instances openstack utilisent 192.168.122.1 comme résolveur DNS.

Voir Network : https://docs.openstack.org/install-guide/launch-instance-networks-provider.html


---------------------------------------------------------------------------------------------------------------
## Les réseaux Selfservice (NEUTRON)
---------------------------------------------------------------------------------------------------------------
### Réseau interne privé (SelfService) :
Le réseau "Selfservcie" se connecte à l'infrastructure réseau physique via NAT. 
Ce réseau comprend un serveur DHCP interne à Openstack qui fournit des adresses IP aux instances qui si connecte. 
Une instance sur ce réseau peut accéder automatiquement à des réseaux externes tels qu'Internet. 
Toutefois, l'accès à une instance sur ce réseau à partir du réseaux externes requiert une adresse IP flottante.

Un utilisateur non privilégié peut créer ce réseau car il fournit uniquement la connectivité aux instances dans son projet.
```bash
$ openstack network create selfservice
$ openstack subnet create --network selfservice --dns-nameserver 8.8.8.8 --gateway 10.0.0.1 --subnet-range 10.0.0.0/24 selfservice
```
*Le réseau libre-service utilise 10.0.0.0/24 avec une passerelle sur 10.0.0.1.
*Un serveur DHCP interne Openstack attribue à chaque instance une IP comprise entre 10.0.0.2 et 10.0.0.254. 
*Toutes les instances utilisent 8.8.8.8 comme résolveur DNS.


Les réseaux en libre-service se connectent aux réseaux des fournisseurs à l'aide d'un routeur virtuel qui effectue un NAT bidirectionnel. Chaque routeur contient une interface sur au moins un réseau en libre-service et une passerelle sur un réseau de Provider.

```bash
$ openstack router create selfrouter
```

Ajoutez le sous-réseau SelfService en tant qu'interface sur le routeur (seflrouter):
```bash
$ openstack router add subnet selfrouter selfservice
```

Définissez la passerelle du routeur "selfrouter" sur le routeur du réseau Provider:
```bash
$ openstack router set selfrouter --external-gateway provider
```

Vérifier l'opération: Répertorier les "namespace" réseau. 
Vous devriez voir un namesapce "qrouter" et deux namespace "qdhcp".
```bash
$ ip netns

qrouter-...
qdhcp-...
qdhcp-...
```

Listez les ports sur le routeur (selfrouter) pour déterminer l'IP de la gateway du réseau PROVIDER: 
```bash
$ openstack port list --router selfrouter
```

Effectuez un ping sur l’ adresse IP à partir du réseau Provider:
```bash
$ ping -c 4 "IP"
```


---------------------------------------------------------------------------------------------------------------
## FLAVORS
---------------------------------------------------------------------------------------------------------------
Le provisionnement de VM demande l'utilisation de modéle (Flavor). 
Créer un FLavors "m1.nano" qui ne nécessite que 64 Mo par instance. Il sera utilisez avec l’image CirrOS à des fins de test.
```bash
$ openstack flavor create --id 0 --vcpus 1 --ram 64 --disk 1 m1.nano
$ openstack flavor list
```

---------------------------------------------------------------------------------------------------------------
## KEYPAIR
---------------------------------------------------------------------------------------------------------------
La plupart des images de cloud prennent en charge l'authentification par clé publique plutôt que l'authentification par mot de passe classique (intégration Cloud-Init).

Générer une paire de clé et ajouter la clé public :
```bash
$ ssh-keygen -q -N ""
$ openstack keypair create --public-key ~/.ssh/id_rsa.pub mykey
```
*Vous pouvez également ignorer la commande ssh-keygen et utiliser une clé publique existante.
*Vous pouvez également ajouter une nouvelle paire de clés via horizon

Vérifiez l'addition de la paire de clés:
```bash
$ openstack keypair list
```

***
openstack keypair create keytest > keytest.pem
chmod 600 keytest.pem
openstack server create ... --key-name keytest ...
openstack server add floating ip ....
ssh -vvv -i ~/.ssh/keytest.pem centos@ipaddress


---------------------------------------------------------------------------------------------------------------
## SECURITY GROUP RULES
---------------------------------------------------------------------------------------------------------------
Le groupe de sécurité default s'applique à toutes les instances et inclut des règles de Firewall qui bloque l'accès distant aux instances. Vous pouvez ajouter vos régles afin d'autoriser par exemple les requetes ICMP (ping) et Secure Shell (SSH).

Afficher les groups de sécurité
```bash
$ openstack security group list
```

Afficher les régles par groupes de sécurité
```bash
$ openstack security group rule list
```

Permettre ICMP (ping):
```bash
$ openstack security group rule create --proto icmp default
```

Permettre l'accès sécurisé au shell (SSH):
```bash
$ openstack security group rule create --proto tcp --dst-port 22 default
```


---------------------------------------------------------------------------------------------------------------
## INSTANCE (NOVA)
---------------------------------------------------------------------------------------------------------------
Pour lancer une instance, vous devez au moins spécifier le "Flavor", l'image, le réseau, le groupe de sécurité, la clé et le nom de l'instance.

Récupérer les informations nécessaires: 
```bash
$ openstack flavor list
$ openstack image list
$ openstack network list
$ openstack security group list
$ openstack keypair list
```


Créer l’instance en spécifiant le réseau rattaché (PROVIDER/SELFSERVICE)
```bash
$ openstack server create --flavor m1.nano --image cirros --nic net-id="NET_ID_SELF" --security-group default --key-name mykey "VM_SELF"
$ openstack server create --flavor m1.nano --image cirros --nic net-id="NET_ID_PROV" --security-group default --key-name mykey "VM_PROV"
```
* Si votre environnement ne contient qu'un seul réseau, vous pouvez omettre l'option "--nic net-id=" car OpenStack choisi automatiquement le seul réseau disponible.


Vérifiez l'état de l’instance:
```bash
$ openstack server list
```
*L'état passe de BUILD à ACTIVE lorsque le processus de génération se termine avec succès.


Vous pouvez afficher les logs d'installation :
```bash
openstack console log show <instance name or ID>
```


---------------------------------------------------------------------------------------------------------------
## VIRTUAL CONSOLE (VNC)
---------------------------------------------------------------------------------------------------------------
Vous pouvez accéder à l'instance via une console virtuelle. Vous devez obtenir l'URL de session VNC (Virtual Network Computing) de l’instance et y accéder depuis un navigateur Web:
```bash
$ openstack console url show "name_VM"
```

A partir de la VM, Vérifiez l'accès à la gataway du réseau physique et l'acces a internet (ping router et site web):
```bash
$ ping -c 4 "IP_VM"
```

Essayer d'accéder aux instances à distance à l'aide de la keypair SSH :
```bash
$ ssh cirros@"IP_VM"
$ ssh -i cloud.key user@ip
```


---------------------------------------------------------------------------------------------------------------
## IP FLOAT
---------------------------------------------------------------------------------------------------------------
*Pour une Instance sur le réseau Self-Service,  il faut créer et associer une adresse IP flottante:
```bash
$ openstack floating ip list
$ openstack floating ip create provider
$ openstack server add floating ip "VM_SELF" "IP_FLOAT"
```

Vérifiez l'état de votre adresse IP flottante:
```bash
$ openstack server list
```

Essayer d'accéder à l'instances "VM_SELF" à distance à l'aide de la keypair SSH :
```bash
$ ssh cirros@"IP_FLOAT_VM_SELF"
```


---------------------------------------------------------------------------------------------------------------
## Les volumes Blocks (CINDER)
---------------------------------------------------------------------------------------------------------------
Créer un volume 
```bash
$ openstack volume create --size 1 nom_vol
```
*après un court moment, l'état du volume doit changer de creating à available


Lister les volumes:
```bash
$ openstack volume list 
```

Afficher les détails du volume:
```bash
$ openstack volume show  nom_vol
```

Attacher le volume à une instance :
```bash
$ openstack server add volume NAME_INSTANCE NAME_VOLUME
```

Accédez à votre instance (SSH) et utilisez fdisk pour vérifier la présence du volume (/dev/vdb) :
```bash
$ sudo fdisk -l
```
*Vous devez créer un système de fichiers sur le périphérique et le monter pour utiliser le volume. 

Migrer le volume vers un nouvel hôte:
```bash
  openstack volume migrate
     - hôte < hôte >
     [ - force - hôte - copie ]
     [ - lock - volume |  - déverrouiller - volume ]
     < volume >
```

Définir les propriétés du volume:
```bash
$ openstack volume set --help
```

Annuler les propriétés de volume
```bash
$ openstack volume unset --help
```

Plus d’information sur les volumes: 
https://docs.openstack.org/python-openstackclient/pike/cli/command-objects/volume.html

détacher un volume:
```bash
openstack server remove volume VM1 volume1
```


---------------------------------------------------------------------------------------------------------------
## Orchestration (HEAT)
---------------------------------------------------------------------------------------------------------------
Les modèles permettent de créer la plupart des types de ressources OpenStack (instances, IP flottantes, volumes, groupes de sécurité,  utilisateurs...). Il fournit également des fonctionnalités avancées telles que la haute disponibilité des instances, la scalability automatique des instances et l'imbrication de stack.
Heat prend en charge d'autre modèle comme "AWS CloudFormation (CFN)". 

Le service d'orchestration comprend les composants suivants:
heat command-line client: CLI qui communique avec l'API "heat-api" pour exécuter les API AWS CloudFormation
heat-api: API REST native OpenStack qui traite les demandes en les envoyant au "heat-engine" via l'appel de procédure distante (RPC).
heat-api-cfn: API AWS Query compatible avec AWS CloudFormation. Il traite les demandes en les envoyant au "heat-engine" via RPC.
heat-api-cloudwatch: Un service API CloudWatch-like pour le projet heat
heat-engine: Orchestre le lancement des templates et fournit des événements au consommateur de l'API.



Les modèles sont définis dans un fichier YAML et suivent la structure suivante:

heat_template_version: 2017-09-01  
Indique que le document YAML est un modèle HOT de la version spécifiée. Il permet d'indiquer non seulement le format du modèle, mais aussi des fonctionnalités qui supportées.  la version peut être soit la date de la version Heat, soit le nom de la version Heat. https://docs.openstack.org/heat/latest/template_guide/hot_spec.html#hot-spec-template-version )

Description: 
Description du templace

Parameter_groups: (facultatif) 
spécifie comment les paramètres d'entrée doivent être regroupés et l'ordre dans lequel ils doivent être fournis sur l'IHM. Ces groupes sont spécifiés dans une liste, chaque groupe contenant une liste de paramètres associés. #Chaque paramètre doit être associé à un groupe spécifique une seule fois à l'aide du nom du paramètre pour le lier à un paramètre défini dans la section des parameters.
```yaml
parameter_groups:
- label: <human-readable label of parameter group>
  description: <description of the parameter group>
  parameters:  #Nom des paramètre défini dans la section des parameters associés
  - <param name>
  - <param name>
```

Parameters: 
spécifie les paramètres d'entrée qui doivent être fournis lors de l'instanciation du modèle. Permet aux utilisateurs de personnaliser un modèle pendant le déploiement. 
 parameters :
 ```yaml
 parameters:
  <param name>:
    type: <string | number | json | comma_delimited_list | boolean>
    label: <human-readable name of the parameter>
    description: <description of the parameter>
    default: <default value for parameter>
    hidden: <true | false>
    constraints:
      <parameter constraints> (length,range,modulo,allowed_values,allowed_pattern,custom_constraint,)
      #voir: https://docs.openstack.org/heat/latest/template_guide/hot_spec.html
    immutable: <true | false> (Définit si le paramètre est modifiable. La mise à jour de pile échoue si elle est définie sur true et que la valeur du paramètre est modifiée. Cet attribut est facultatif et sa valeur par défaut est false)
    tags: <list of parameter categories>
 ```

Resources:  
contient la déclaration des ressources à déployer. 
Chaque ressource est définie comme un bloc distinct. 
En fonction du type de ressource, le bloc de ressources peut inclure davantage de données spécifiques aux ressources.
resources
```yaml
  <resource ID>:
    type: <resource type> (OS::Nova::Server or OS::Neutron::Port...)
    properties:
      <property name>: <property value>
    metadata:
      <resource specific metadata>
    depends_on: <resource ID or list of ID> (Dépendances de la ressource sur une ou plusieurs ressources du modèle)
    update_policy: <update policy> (Stratégie de mise à jour de la ressource)
    deletion_policy: <deletion policy> (Stratégie de suppression de la ressource. Delete , Retain et Snapshot)
    external_id: <external resource ID> (Permet de spécifier l'id de la ressource pour une ressource externe existante)
    condition: <condition name or expression or boolean> (Condition pour la ressource. Qui décide de créer ou non la ressource.)
   ```

Outputs:   
Permet de spécifier les paramètres de sortie disponibles pour les utilisateurs une fois le modèle instancié.
```yaml
outputs:
  <parameter name>:
    description: <description>
    value: <parameter value> (Cette valeur est généralement résolue au moyen d'une fonction. )
    condition: <condition name or expression or boolean> (Pour définir de manière conditionnelle une valeur de sortie. Aucune valeur ne sera affichée si la condition est False.)
```

Conditions: 
(facultatif) La section conditions définit une ou plusieurs conditions évaluées en fonction des valeurs de paramètre d'entrée fournies lorsqu'un utilisateur crée ou met à jour une pile. La condition peut être associée à des ressources, des propriétés de ressources et des sorties. Elle inclut des instructions qui peuvent être utilisées pour restreindre la création d'une ressource ou la définition d'une propriété de la section resources et peuvent également être associés à des sorties dans les sections outputs.
Par exemple, en fonction du résultat d'une condition, l'utilisateur peut créer de manière conditionnelle des ressources, l'utilisateur peut définir de manière conditionnelle différentes valeurs de propriétés, et l'utilisateur peut conditionnellement donner les résultats d'une pile.
conditions:
```yaml
  <condition name1>: {expression1}
  <condition name2>: {expression2}
  ...
```

Outre les paramètres définis par l'auteur du modèle, Heat crée également trois paramètres pour chaque pile, permettant un accès référentiel au nom de la pile, à l'identificateur de la pile et à l'identificateur du projet. Ces paramètres sont nommés OS::stack_name pour le nom de la pile, OS::stack_id pour l'identificateur de pile et OS::project_id pour l'identificateur de projet. Ces valeurs sont accessibles via la fonction intrinsèque get_param , tout comme les paramètres définis par l'utilisateur.

Vous devez ajouter le rôle heat_stack_owner à chaque utilisateur qui gère les Stack:
Ajoutez le rôle heat_stack_owner au projet et à l'utilisateur pour permettre la gestion de la Stack par l'utilisateur:
```bash
$ openstack role add --project projet1 --user user1 heat_stack_owner  
```

Le service d'orchestration attribue automatiquement le rôle "heat_stack_user" aux utilisateurs qu'il crée lors du déploiement. Par défaut, ce rôle restreint les opérations de l'API. Pour éviter les conflits, n’ajoutez pas ce rôle aux utilisateurs dotés du rôle "heat_stack_owner".



Le service Orchestration utilise des template pour décrire les stacks. 
Créer le fichier demo-template.yml:
```yaml
heat_template_version: 2018-08-31

description: >
   Simple template to deploy a single compute instance
   Un modèle est plus facilement réutilisable en définissant un ensemble de paramètres d'entrée au lieu de coder en dur ces valeurs.

parameter_groups:
- label: GROUP1
  description: Configuration de l'instane
  parameters:
  - flavor_type
  - key_name
  - image_id
  #- NetID

parameters:
    #Ici, trois paramètres d'entrée sont définis et doivent être fournis par l'utilisateur lors du déploiement
  key_name:
    type: string
    label: Key Name
    description: Name of key-pair to be used for compute instance
    #on défini la valeur par défaut à utiliser si l'utilisateur ne fournit pas de paramètre d'entrée
    default: mykey
    #on masque la valeur du parametre lorsque les utilisateurs demandent des informations sur une Stack déployée à partir d'un modèle (Ex: caché password)
    hidden: true
  image_id:
    type: string
    label: Image ID
    description: Image to be used for compute instance
    default: CentOS-7
  flavor_type: 
    type: string
    label: Instance Type
    description: Type of instance (flavor) to be used
    default: m3.std
    #On limite les valeurs des paramètres d'entrée que les utilisateurs peuvent fournir. 
    constraints:
      - allowed_values: [ m3.std , m3.big ]
        description: La valeur doit être m3.std ou m3.big.
  #NetID:
  #  type: string
  #  description: Network ID to use for the instance.

  #définir une liste de contraintes qui doivent toutes être remplies par les entrées de l'utilisateur:
  #Exemple: exigences de format d'un mot de passe à fournir par les utilisateurs
  #database_password:
    #type: string
    #label: Database Password
    #description: Password to be used for database
    #hidden: true
    #constraints:
    #  - length: {  min : 6 ,  max : 8 }
    #    description: Password length must be between 6 and 8 characters.
    #  - allowed_pattern: "[a-zA-Z0-9]+"
    #    description: Password must consist of characters and numbers only.
    #  - allowed_pattern: "[A-Z]+[a-zA-Z0-9]*"
    #   description: Password must start with an uppercase character.

resources:
  private_net:
    type: OS::Neutron::Net
    properties:
      name: frontend

  private_subnet:
    type: OS::Neutron::Subnet
    properties:
      name: subfront
      network_id: { get_resource: private_net }
      #network_id: frontend
      cidr: 10.0.100.0/24
      gateway_ip: 10.0.100.1
      allocation_pools:
        - start: 10.0.100.2
          end: 10.0.100.253

  router:
    type: OS::Neutron::Router
    properties:
      name: gateway
      external_gateway_info:
        network: provider

  router_interface:
    type: OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: router }
      subnet_id: { get_resource: private_subnet }

  server1_port:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: private_net }
      fixed_ips:
        - subnet_id: { get_resource: private_subnet }

  my_instance:
    type: OS::Nova::Server
    properties:
      name: VM1
      #Les valeurs fixes pour les propriétés de ressources respectives ont été remplacées par des références aux paramètres d'entrée correspondants 
      #au moyen de la fonction get_param
      key_name: { get_param: key_name }
      image: { get_param: image_id }
      flavor: { get_param: flavor_type }
      networks:
      #- network: { get_param: NetID }
      - port: { get_resource: server1_port }

  server1_floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: provider
      port_id: { get_resource: server1_port }

### SORTIE UTILISATEUR (Résultat): 
outputs:
  server1_private_ip:
    description: IP address of my_instance in private network
    value: { get_attr: [ my_instance, first_address ] }
  server1_public_ip:
    description: Floating IP address of server1 in public network
    value: { get_attr: [ server1_floating_ip, floating_ip_address ] }
```




Créez une stack à partir du template sur le réseau définit:
```bash
$ openstack stack create -t demo.yml --parameter NetID="NET_ID" mystack
```
*L'option "--parameter xxx=" permet de renseingé les paramétre utilisateur requis

vérifiez la création de la stack:
```bash
$ openstack stack list
```

Affiche les informations de sortie "voutputs" de la stack:
```bash
$ openstack stack output show --all stack
$ openstack server list
```

Supprimer la stack:
```bash
$ openstack stack delete --yes stack
```

Plus d’information sur l’orchestration : https://docs.openstack.org/heat/latest/template_guide/hot_spec.html




---------------------------------------------------------------------------------------------------------------
##  Shared File Systems service (MANILA) 
---------------------------------------------------------------------------------------------------------------
Le service OpenStack Shared File Systems (manila) fournit un accès coordonné aux systèmes de fichiers partagés ou distribués. La méthode dans laquelle le partage est provisionné et utilisé est déterminée par le pilote du système de fichiers partagés, ou des pilotes dans le cas d'une configuration multi-backend. De nombreux pilotes prennent en charge NFS, CIFS, HDFS, GlusterFS, CEPHFS, MAPRFS et d’autres protocoles.
Les services d'API et de planificateur de systèmes de fichiers partagés s'exécutent généralement sur les nœuds de contrôleur. Selon les pilotes utilisés, le service de partage peut s'exécuter sur des contrôleurs, des nœuds de calcul ou des nœuds de stockage.

https://docs.openstack.org/mitaka/config-reference/shared-file-systems/drivers.html

Pour des raisons de simplicité, ce guide décrit la configuration du service Systèmes de fichiers partagés pour utiliser l’un des éléments suivants:

- Un back-end generic avec le mode DHSS (driver_handles_share_servers) activé, qui utilise les  services : Compute( Nova ), image (Glance), réseau (Neutron) et stockage en bloc (Cinder).
- LVM avec le mode DHSS (driver_handles_share_servers) désactivé.

Le protocole de stockage utilisé et référencé utilisé est NFS . Comme indiqué ci-dessus, le service de système de fichiers partagé prend en charge différents protocoles de stockage en fonction de l'arrière-plan choisi.




---------------------------------------------------------------------------------------------------------------
## LOAD BALANCER (LBaaS)
---------------------------------------------------------------------------------------------------------------
Les agents gèrent la configuration HAProxy et gèrent le démon HAProxy. configurer plusieurs ports d'écoute sur une seule adresse IP d'équilibreur de charge.


Créer un Load Balancer sur un réseau :
$ neutron lbaas-loadbalancer-create --name NAME_LB  NAME_SUBNET

Afficher l'état du Load Balancer :
$ neutron lbaas-loadbalancer-list
$ neutron lbaas-loadbalancer-show NAME_LB  

Créez un nouveau groupe de sécurité avec des règles d'entrée pour autoriser le trafic dans le nouvel équilibreur de charge. autoriser le port TCP 80, le port TCP 443 et tout le trafic ICMP:
$ neutron security-group-create NAME_LBaaS
$ neutron security-group-rule-create --direction ingress --protocol tcp --port-range-min 80 --port-range-max 80 --remote-ip-prefix 0.0.0.0/0 NAME_LBaaS
$ neutron security-group-rule-create --direction ingress --protocol tcp --port-range-min 443 --port-range-max 443 --remote-ip-prefix 0.0.0.0/0 NAME_LBaaS
$ neutron security-group-rule-create --direction ingress --protocol icmp NAME_LBaaS

Appliquez le groupe de sécurité au port réseau de l'équilibreur de charge :
$ neutron port-update --security-group NAME_LBaaS 9f8f8a75-a731-4a34-b622-864907e1d556
* L’équilibreur de charge est actif et prêt à traiter le trafic. (Vérifiez que l'équilibreur de charge répond aux pings avant de continue).

Ajouter un Listener HTTP :
$ neutron lbaas-listener-create --name LISTER-NAME-HTTP --loadbalancer LBaaS --protocol HTTP --protocol-port 80

Créer un pool :
$ neutron lbaas-pool-create --name POOL-NAME-HTTP --lb-algorithm ROUND_ROBIN --listener LISTER-NAME-HTTP --protocol HTTP
* Dans cet exemple, l'équilibreur de charge utilise l'algorithme Round Robin et le trafic alterne entre les serveurs Web du backend.
Ajouter des membres au pool pour diffuser le contenu HTTP au port 80 :
$ neutron lbaas-member-create --subnet private-subnet --address 192.168.1.16 --protocol-port 80 POOL-NAME-HTTP
$ neutron lbaas-member-create --subnet private-subnet --address 192.168.1.17 --protocol-port 80 POOL-NAME-HTTP

vérifier la connectivité via les équilibreurs de charge sur vos serveurs Web:
$ curl IP_SERVER_WEB

Moniteur d'intégrité
Ajouter un moniteur d'intégrité de sorte que les serveurs qui ne répondent pas soient supprimés du pool:
$ neutron lbaas-healthmonitor-create --delay 5 --max-retries 2 --timeout 10 --type HTTP --pool POOL-NAME-HTTP
* Dans cet exemple, le moniteur d'intégrité supprime le serveur du pool s'il échoue à un contrôle d'intégrité à deux intervalles de cinq secondes. Lorsque le serveur récupère et recommence à répondre aux vérifications de l'état, il est à nouveau ajouté au pool.

Ajouter un Listener HTTPS (443) 
Ajouter un autre port d'écoute sur le port 443 pour le trafic HTTPS. LBaaS offre une terminaison SSL / TLS sur l'équilibreur de charge,  mais cet exemple adopte une approche permettant aux connexions chiffrées de se terminer sur chaque serveur membre du pool.
$ neutron lbaas-listener-create --name LISTER-NAME-HTTPS --loadbalancer NAME_LB  --protocol HTTPS --protocol-port 443
$ neutron lbaas-pool-create --name POOL-NAME-HTTPS --lb-algorithm LEAST_CONNECTIONS --listener LISTER-NAME-HTTPS --protocol HTTPS
$ neutron lbaas-member-create --subnet private-subnet --address 192.168.1.16 --protocol-port 443 POOL-NAME-HTTPS
$ neutron lbaas-healthmonitor-create --delay 5 --max-retries 2 --timeout 10 --type HTTPS --pool POOL-NAME-HTTPS
Les équilibreurs de charge déployés sur un réseau public ou fournisseur accessibles aux clients externes n'ont pas besoin d'une adresse IP flottante affectée. Les clients externes peuvent directement accéder à l'adresse IP virtuelle (VIP) de ces équilibreurs de charge.
Cependant, les équilibreurs de charge déployés sur des réseaux privés ou isolés ont besoin d'une adresse IP flottante attribuée s'ils doivent être accessibles aux clients externes. Pour compléter cette étape, vous devez disposer d'un routeur entre les réseaux privé et public et d'une adresse IP flottante disponible.

Localiser vip_port_id  (Vip_port_id est l'ID du port réseau affecté à l'équilibreur de charge.)
$ neutron lbaas-loadbalancer-show ID_load-balancer
$ neutron floatingip-associate FLOATINGIP_ID LOAD_BALANCER_PORT_ID


Récupération des statistiques du Load Balancer :
L'agent LBaaS v2 collecte quatre types de statistiques pour chaque équilibreur de charge toutes les six secondes.
$ neutron lbaas-loadbalancer-stats LISTER-NAME-HTTP
* Le nombre de connexions active_connections correspond au nombre total de connexions actives au moment où l'agent a interrogé l'équilibreur de charge. Les trois autres statistiques sont cumulatives depuis le dernier démarrage de l'équilibreur de charge. Par exemple, si l'équilibreur de charge redémarre en raison d'une erreur système ou d'un changement de configuration, ces statistiques seront réinitialisées.
















9.  MAGNUM
10. vous pouvez provisionner des clusters de conteneur composés de machines virtuelles ou de serveurs Baremetal. vous allez créer un modèle de cluster pour un COE spécifique, puis vous provisionnerez un cluster à l'aide du modèle de cluster correspondant. Ensuite, vous pouvez utiliser le client COE approprié ou le point de terminaison pour créer des conteneurs.

Create an external network with an appropriate provider based on your cloud provider support for your case:
$ openstack network create public --provider-network-type vxlan --external --project service
$ openstack subnet create public-subnet --network public --subnet-range 192.168.1.0/24                                --gateway 192.168.1.1 --ip-version 4

To create a magnum cluster, you need a keypair which will be passed in all compute instances of the cluster. If you don’t have a keypair in your project, create one.

$ openstack keypair create --public-key ~/.ssh/id_rsa.pub mykey

The VM versions of Kubernetes and Docker Swarm drivers require a Fedora Atomic image. The following is stock Fedora Atomic image, built by the Atomic team and tested by the Magnum team.

$ wget https://fedorapeople.org/groups/magnum/fedora-atomic-latest.qcow2
$ openstack image create --disk-format=qcow2 --container-format=bare --file=fedora-atomic-latest.qcow2 --property os_distro='fedora-atomic' fedora-atomic-latest

Créez un modèle de cluster pour un cluster Docker Swarm :
openstack coe cluster template create swarm-cluster-template --image fedora-atomic-latest  --external-network public --dns-nameserver 8.8.8.8 --master-flavor m1.standard --flavor m1.standard --coe swarm

Créez un cluster avec un nœud et un maître :
openstack coe cluster create swarm-cluster --cluster-template swarm-cluster-template --master-count 1 --node-count 1 --keypair rootkey

Votre cluster est en cours de création. Le temps de création dépend des performances de votre infrastructure. Vous pouvez vérifier l'état de votre cluster :

openstack coe cluster list
openstack coe cluster show swarm-cluster

Ajoutez les informations d'identification du cluster ci-dessus à votre environnement:

mkdir myclusterconfig
$(openstack coe cluster config swarm-cluster --dir myclusterconfig)
La commande ci-dessus va sauvegarder les artefacts d'authentification dans le répertoire myclusterconfig et exportera les variables d'environnement: DOCKER_HOST, DOCKER_CERT_PATH et DOCKER_TLS_VERIFY. Sortie de l'échantillon:

export DOCKER_HOST=tcp://172.24.4.10:2376
export DOCKER_CERT_PATH=myclusterconfig
export DOCKER_TLS_VERIFY=True
Create a container:
$ docker run busybox echo "Hello from Docker!"
Delete the cluster:
$ openstack coe cluster delete swarm-cluster




Provision a Kubernetes cluster
Vous allez provisionner un cluster Kubernetes avec un maître et un nœud. Ensuite, en utilisant le client natif de kubectl , kubectl , vous allez créer un déploiement.


Créez un modèle de cluster pour un cluster Kubernetes 
openstack coe cluster template create kubernetes-cluster-template  --image fedora-atomic-latest --external-network provider --dns-nameserver 8.8.8.8 --master-flavor m1.big --flavor m1.big --coe kubernetes

Créez un cluster avec un nœud et un maître :
openstack coe cluster create kubernetes-cluster --cluster-template kubernetes-cluster-template --master-count 1 --node-count 1 --keypair rootkey
Votre cluster est en cours de création. Le temps de création dépend des performances de votre infrastructure. Vous pouvez vérifier l'état de votre cluster en utilisant les commandes: 
openstack coe cluster list
openstack coe cluster show kubernetes-cluster

Ajoutez les informations d'identification du cluster:
$ mkdir -p ~/clusters/kubernetes-cluster
$ $(openstack coe cluster config kubernetes-cluster --dir ~/clusters/kubernetes-cluster)
La commande ci-dessus sauvegardera les artefacts d'authentification dans le répertoire ~/clusters/kubernetes-cluster et exportera la variable d'environnement KUBECONFIG :
export KUBECONFIG=/home/user/clusters/kubernetes-cluster/config

Vous pouvez lister les composants du contrôleur de votre cluster Kubernetes et vérifier s'ils sont en Running :
$ kubectl -n kube-system get po

 créer un déploiement nginx et vérifier qu'il est en cours d'exécution:
$ kubectl run nginx --image=nginx --replicas=5

Delete the cluster:
$ openstack coe cluster delete kubernetes-cluster