#Classe CINDER
class controller::cinder_server {
  if $controller::manage_cinder_server {
    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0644',
      owner   => '0',
      backup  => '.puppet-bak',
      notify  => Service[$controller::service_cinder_server],
    }
      #file { '/var/tmp/install_cinder_server.sh' :
        #content => epp('controller/install_cinder_server.epp'),
        #mode    => '0744',
      #}

    package { $controller::package_cinder_server :
      ensure => 'present',
      notify => File[$controller::file_cinder_sudo],
    }
      file { $controller::file_cinder_sudo :
        content => epp('controller/cinder_sudo.epp'),
        mode    => '0440',
      }

      #file { $controller::file_cinder_server :
        #content => epp('controller/cinder.epp'),
        #require => Package[$controller::package_cinder_server],
        #notify  => Exec['config_cinder'],
      #}
        #exec { 'config_cinder' :
          #command     => '/var/tmp/install_cinder_server.sh',
          #require     => File['/var/tmp/install_cinder_server.sh'],
          #notify      => Service[$controller::service_cinder_server],
          #refreshonly => true,
        #}

    service { $controller::service_cinder_server :
      ensure => 'running',
      enable => true,
    }
  }
  else {
    package { $controller::package_cinder_server :
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_cinder_server : }
  }
}
