#Classe CINDER
class controller::cinder_controller {
  if $controller::manage_cinder_controller {
    File {
      ensure  => 'present',
      group   => 'cinder',
      mode    => '0640',
      owner   => '0',
      backup  => '.puppet-bak',
    }
      file { '/var/tmp/install_cinder.sh' :
        content => epp('controller/install_cinder.epp'),
        group   => '0',
        mode    => '0744',
      }

    package { $controller::package_cinder_controller :
      ensure => 'present',
      notify => File[$controller::file_cinder_controller],
    }

      file { $controller::file_cinder_controller :
        content => epp('controller/cinder.epp'),
        require => Package[$controller::package_cinder_controller],
        notify  => Exec['config_cinder'],
      }
        exec { 'config_cinder' :
          command     => '/var/tmp/install_cinder.sh',
          require     => File[$controller::file_cinder_controller],
          notify      => Service[$controller::service_cinder_controller],
          refreshonly => true,
        }

    service { $controller::service_cinder_controller :
      ensure => 'running',
      enable => true,
    }
  }
  else {
    package { $controller::package_cinder_controller :
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_cinder_controller : }
  }
}
