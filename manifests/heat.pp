#Classe HEAT
class controller::heat{

  if $controller::manage_heat {
    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0644',
      owner   => '0',
      backup  => '.puppet-bak',
    }
    file { '/var/tmp/install_heat.sh' :
      content => epp('controller/install_heat.epp'),
      mode    => '0740',
    }

    package { $controller::package_heat :
      ensure => 'present',
      notify => Exec['config_heat'],
    }
      exec { 'config_heat' :
        command     => '/var/tmp/install_heat.sh',
        require     => File[$controller::file_heat],
        notify      => Service[$controller::service_heat],
        refreshonly => true,
      }
      file { $controller::file_heat :
        content => epp('controller/heat.epp'),
        group   => 'heat',
        require => Package[$controller::package_heat],
      }

    service { $controller::service_heat :
      ensure    => 'running',
      enable    => true,
      subscribe => File[$controller::file_heat],
      require   => Package[$controller::package_heat],
    }
  }
  else {
    package { $controller::package_nova_heat :
      ensure => 'absent',
    }
    File{
      ensure => 'absent',
    }
      file { $controller::file_nova_heat : }
      file { '/var/tmp/install_heat.sh' : }
  }
}




