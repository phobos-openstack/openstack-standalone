#Classe principale
class controller (

#### init openstack ####
Boolean $manage_install,
Array[String] $package_repo,
String  $upgrade_repo,
Boolean $manage_networkmanager,
Array[String] $package_openstack,
String $file_br_mgt,
String $file_br_ex,
String $ip_mgt,

######### Chrony ########
Boolean $manage_chrony,
Array[String] $package_chrony,
String $file_chrony,
String $chrony_server,
String $chrony_allow,
Array[String] $service_chrony,

######### MONGODB ########
Boolean $manage_mongodb,
Array[String] $package_mongodb,
Array[String] $service_mongodb,
String $file_mongodb,

######### MARIADB ########
Boolean $manage_mariadb,
Array[String] $package_mariadb,
#String $openstack_install_provider,
#String $openstack_install_options,
Array[String] $service_mariadb,
String $file_mariadb,

######### RABBITMQ ########
Boolean $manage_rabbitmq,
Array[String] $package_rabbitmq,
Array[String] $service_rabbitmq,
String $file_rabbitmq,

######## MEMCACHED #######
Boolean $manage_memcached,
Array[String] $package_memcached,
Array[String] $service_memcached,
String $file_memcached,

######### ETCD ########
Boolean $manage_etcd,
Array[String] $package_etcd,
Array[String] $service_etcd,
String $file_etcd,

######### HTTPD #######
Boolean $manage_httpd,
Array[String] $package_httpd,
String $service_httpd,
String $file_httpd,

######### KEYSTONE #######
Boolean $manage_keystone,
Array[String] $package_keystone,
String $file_keystone,
String $file_admin_openrc,

######### GLANCE ########
Boolean $manage_glance,
Array[String] $package_glance,
Array[String] $service_glance,
String $file_glance_api,
String $file_glance_registry,

######### PLACEMENT API ########
Boolean $manage_placement,
Array[String] $package_placement,
String $file_placement,
String $file_placement_api,

######### NOVA CONTROLLER ########
Boolean $manage_nova,
Array[String] $package_nova_controller,
String $file_nova_controller,
String $ip_mgt_nova,
String $api_database_nova,
String $database_nova,
String $mq_url,
#String $vnc_server_listen,
Array[String] $service_nova_controller,

######### HEAT ########
Boolean $manage_heat,
Array[String] $package_heat,
String $file_heat,
Array[String] $service_heat,


######### HORIZON ########
Boolean $manage_horizon,
Array[String] $package_horizon,
String $file_horizon,
String $file_horizon_setting,
String $horizon_webservice,


###########################################################################
############################ NEUTRON SERVER ###############################
###########################################################################
Boolean $manage_neutron_server,
Array[String] $package_neutron_server,
String $file_neutron_server,
String $file_ml2_conf,
String $ml2_mechanism_drivers,
String $file_linuxbridge_agent,
String $physical_interface_mappings,
String $overlay_ip,
String $file_l3_agent,
String $l3_interface_driver,
String $file_dhcp_agent,
String $dhcp_interface_driver,
String $file_metadata_agent,
Array[String] $service_neutron_server,
String $network_name,
String $network_start,
String $network_end,
String $network_dns,
String $network_gateway,
String $network_range,
String $network_subnet_name,


###########################################################################
############################# COMPUTE SERVER ##############################
###########################################################################
Boolean $manage_compute,
Array[String] $package_compute,
#String $file_nova_compute,
#String $ip_mgt_nova_compute,
#String $mq_url,
String $vnc_server_listen,
String $vnc_novncproxy_base_url,
String $virt_type,
String $discover_hosts_in_cells_interval,
#String $file_neutron_compute,

Array[String] $service_compute,


###########################################################################
############################## CINDER (BLOCK) #############################
###########################################################################
# Configure CINDER CONTROLLER:
Boolean $manage_cinder_controller,
Array[String] $package_cinder_controller,
String $file_cinder_controller,
Array[String] $service_cinder_controller,

# Configure CINDER SERVER:
Boolean $manage_cinder_server,
Array[String] $package_cinder_server,
#String $file_cinder_server,
String $file_cinder_sudo,
Array[String] $service_cinder_server,
String $cinder_volumes,

# Configure CINDER BACKUP SERVICE:
Boolean $manage_cinder_backup,
#String $package_cinder_backup,
#String $file_cinder_backup,
String $backup_swift_url,
String $backup_driver,
String $service_cinder_backup,

# Script Delete All
Boolean $manage_delete_all,




###########################################################################
############################## SWIFT (OBJECT) #############################
###########################################################################
# Configure SWIFT PROXY:
Boolean $manage_swift_proxy,
Array[String] $package_swift_proxy,
#String $file_swift_storage_swift,
String $file_swift_proxy,
String $memcache_servers_proxy_swift,
Array[String] $service_swift_proxy,

# Configure SWIFT STOCKAGE RSYNC:
Boolean $manage_swift_storage,
Array[String] $package_swift_storage,
String $file_swift_storage_rsyncd,
String $file_swift_storage_default_rsync,
String $ip_mgt_rsync_swift,
#Copiez le fichier swift.conf dans le répertoire /etc/swift de chaque nœud de stockage et de tout nœud supplémentaire exécutant le service de proxy.
String $file_swift_storage_swift,
String $swift_hash_path_suffix,
String $swift_hash_path_prefix,
String $file_swift_storage_account,
String $file_swift_storage_container,
String $file_swift_storage_object,
Array[String] $service_swift_storage,
String $swift_stockage_bind_ip,
Array[String] $swift_device_name,
String $swift_device_weight,
String $swift_stockage_devices,



###########################################################################
############################## SHARE (MANILA) #############################
###########################################################################
### MANILA CONTROLLER
Boolean $manage_manila_controller,
Array[String] $package_manila_controller,
String $file_manila_controller,
String $manila_default_share_type,
String $manila_ip_mgt,
Array[String] $service_manila_controller,


### MANILA SHARE NODE
Boolean $manage_manila_share_node,
#Array[String] $package_manila_share_node,
#String $file_manila_share_node,
#String $manila_ip_mgt,
#service_image_name , service_instance_flavor_id , service_instance_user et service_instance_password font référence à l'image de service utilisée par le pilote pour créer des serveurs de partage. 
#Un exemple d'image de service à utiliser avec le pilote generic est disponible dans le projet manila-image-elements .

String $enabled_share_backends,
String $enabled_share_protocols,
String $share_backend_name,
String $service_instance_flavor_id,
String $service_image_name,
#Vous pouvez également utiliser des clés SSH au lieu de l'authentification par mot de passe pour les informations d'identification de l'instance de service.
String $service_instance_user,
String $service_instance_password,
Array[String] $service_manila_share_node,
)
{
  contain controller::chrony
  contain controller::install
  contain controller::mongodb
  contain controller::mariadb
  contain controller::rabbitmq
  contain controller::memcached
  contain controller::etcd
  contain controller::httpd
  contain controller::keystone
  contain controller::glance
  contain controller::placement
  contain controller::nova
  contain controller::heat
  contain controller::horizon

  #Network server (NEUTRON):
  contain controller::neutron_server

  #Block server (CINDER):
  contain controller::cinder_controller
  contain controller::cinder_server
  contain controller::cinder_backup

  #Object server (SWIFT):
  contain controller::swift_proxy
  contain controller::swift_storage

  #Compute server:
  contain controller::compute

  #Shared File Systems service (manila) :
  contain controller::manila_controller
  contain controller::manila_share_node

  #Delete ALLL
  contain controller::delete_all


#Config Network:
#file { '/etc/sysconfig/network-scripts/ifcfg-ens192'
#file { '/etc/sysconfig/network-scripts/ifcfg-ens224'

#Actions Mariadb:
#/usr/bin/mysql_secure_installation
}

