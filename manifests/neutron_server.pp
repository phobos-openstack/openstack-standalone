#Classe NEUTRON
class controller::neutron_server{

  if $controller::manage_neutron_server {
    File {
      ensure  => 'present',
      group   => 'neutron',
      mode    => '0640',
      owner   => '0',
    }
    file { '/var/tmp/config_network_neutron.sh' :
      content => epp('controller/config_network_neutron.epp'),
      mode    => '0744',
      group   => '0',
    }
    file { '/var/tmp/install_neutron_server.sh' :
      content => epp('controller/install_neutron_server.epp'),
      mode    => '0744',
      group   => '0',
    }
    package { $controller::package_neutron_server :
      ensure => 'present',
      notify => Exec['config_neutron'],
    }
      exec { 'config_neutron' :
        command     => '/var/tmp/install_neutron_server.sh',
        require     => File[
          $controller::file_neutron_server,
          $controller::file_ml2_conf,
          $controller::file_linuxbridge_agent,
          $controller::file_l3_agent,
          $controller::file_dhcp_agent,
          $controller::file_metadata_agent,
        ],
        notify      => Service[$controller::service_neutron_server],
        refreshonly => true,
      }

    file { $controller::file_neutron_server :
      content => epp('controller/neutron_server.epp'),
      require => Package[$controller::package_neutron_server],
      notify  => Service[$controller::service_neutron_server],
    }
    file { $controller::file_ml2_conf :
      content => epp('controller/neutron_ml2_conf.epp'),
      require => Package[$controller::package_neutron_server],
      notify  => Service[$controller::service_neutron_server],
    }
    file { $controller::file_linuxbridge_agent :
      content => epp('controller/neutron_linuxbridge_agent.epp'),
      require => Package[$controller::package_neutron_server],
      notify  => Service[$controller::service_neutron_server],
    }
    file { $controller::file_l3_agent :
      content => epp('controller/neutron_l3_agent.epp'),
      require => Package[$controller::package_neutron_server],
      notify  => Service[$controller::service_neutron_server],
    }
    file { $controller::file_dhcp_agent :
      content => epp('controller/neutron_dhcp_agent.epp'),
      require => Package[$controller::package_neutron_server],
      notify  => Service[$controller::service_neutron_server],
    }
    file { $controller::file_metadata_agent :
      content => epp('controller/neutron_metadata_agent.epp'),
      require => Package[$controller::package_neutron_server],
      notify  => Service[$controller::service_neutron_server],
    }

    service { $controller::service_neutron_server :
      ensure => 'running',
      enable => true,
      notify => Exec['config_network'],
    }
      exec { 'config_network' :
        command     => '/var/tmp/config_network_neutron.sh',
        refreshonly => true,
      }
  }
  else {
    package { $controller::package_neutron_server :
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { '/var/tmp/install_neutron_server.sh' : }
      file { $controller::file_neutron_server : }
      file { $controller::file_ml2_conf : }
      file { $controller::file_linuxbridge_agent : }
      file { $controller::file_l3_agent : }
      file { $controller::file_dhcp_agent : }
      file { $controller::file_metadata_agent : }

    exec { '/bin/rm -f /etc/neutron/plugin.ini' : }
    #exec { '/bin/echo "drop database neutrondb" | /bin/mysql -u root' : }
  }
}




