# Installation placement
class controller::placement {
  if $controller::manage_placement {
    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0640',
      owner   => '0',
      backup  => '.puppet-bak',
    }
    file { '/var/tmp/install_placement.sh' :
      content => epp('controller/install_placement.epp'),
      mode    => '0740',
    }
    package { $controller::package_placement :
      ensure => 'present',
      before => File[$controller::file_placement],
      notify => Exec['config_placement'],
    }
      exec { 'config_placement' :
        command => '/var/tmp/install_placement.sh',
        require => File[$controller::file_placement],
      }

    file { $controller::file_placement :
      content => epp('controller/placement.epp'),
      group   => 'placement',
      mode    => '0644',
    }
    file { $controller::file_placement_api :
        content => epp('controller/placement-api.epp'),
        group   => '0',
        require => Package[$controller::package_nova_controller],
        notify  => Exec['config_placement'],
    }
  }
}
