#Classe manila_share_node
class controller::manila_share_node{
  if $controller::manage_manila_share_node{
    File {
      ensure  => 'present',
      group   => 'manila',
      mode    => '0640',
      owner   => '0',
      #backup  => '.puppet-bak',
    }

    #package { $controller::package_manila_share_node:
      #ensure => 'present',
      #notify => File[$controller::file_manila_share_node],
    #}
      #file { $controller::file_manila_share_node:
        #content => epp('controller/manila_share_node.epp'),
        #require => Package[$controller::package_swift_proxy],
      #}

    service { $controller::service_manila_share_node:
      ensure    => 'running',
      enable    => true,
      subscribe => File[$controller::file_manila_share_node],
    }
  }
  else {
    package { $controller::package_manila_share_node:
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_manila_share_node: }
  }
}
