#Classe COMPUTE
class controller::compute{

  if $controller::manage_compute {
    package { $controller::package_compute :
      ensure => 'present',
      #before => File['/etc/nova/nova.conf'],
    }

    File {
      ensure => 'present',
      group  => 'root',
      mode   => '0644',
      owner  => 'root',
    }
    #file { $controller::file_nova_compute:
      #content => epp('controller/nova.epp'),
      #require => Package[$controller::package_compute],
      #notify  => Service[$controller::service_compute],
    #}
    #file { $controller::file_neutron_compute :
      #content => epp('controller/neutron.epp'),
      #require => Package[$controller::package_compute],
      #notify  => Service[$controller::service_compute],
    #}
    #file { $controller::file_linuxbridge_agent :
      #content => epp('controller/neutron_linuxbridge_agent.epp'),
    #}

    #sysctl:
    #net.bridge.bridge-nf-call-iptables
    #net.bridge.bridge-nf-call-ip6tables
    #br_netfilter

    service { $controller::service_compute :
      ensure => 'running',
      enable => true,
    }
  }
  else {
    package { $controller::package_compute :
      ensure => 'absent',
    }
    File { $controller::file_nova_compute :
      ensure => 'absent',
    }
  }
}
