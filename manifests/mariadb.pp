#Classe mariadb
class controller::mariadb {

  if $controller::manage_mariadb {
    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0644',
      owner   => '0',
      backup  => '.puppet-bak',
    }
    file { '/var/tmp/install_db_openstack.sql' :
      content => epp('controller/install_db_openstack.sql.epp'),
      mode    => '0744',
    }

    package { $controller::package_mariadb :
      ensure => 'present',
      #before          => File[$controller::file_mariadb],
      # Before (Avant): Applique une ressource avant la ressource cible
      notify => Exec['config_maria'],
      # Notify: Applique une ressource avant la ressource cible. 
      # La cible est actualisée si la ressource notifiante est modifiée
    }
      exec { 'config_maria' :
        command     => '/bin/cat /var/tmp/install_db_openstack.sql | /bin/mysql -u root',
        require     => Service[$controller::service_mariadb],
        refreshonly => true,
        # refreshonly: Ne s'applique uniquement que si une autre ressource le notifie.
        # onlyif, unless, or creates: Si l'exec a déjà exécuté et reçoit une deuxieme notification, 
        #il exécutera sa commande jusqu'à deux fois. 
        #(Si une condition onlyif, unless, or creates, n'est plus remplie après la première exécution, 
        # la deuxième exécution ne se produira pas.)  
      }

    file { $controller::file_mariadb :
      content => epp('controller/mariadb.epp'),
      #require => Package[$controller::package_mariadb],
      #require: Applique une ressource après la ressource cible
      notify  => Service[$controller::service_mariadb],
    }

    service { $controller::service_mariadb :
      ensure    => 'running',
      enable    => true,
      before    => Exec['config_maria'],
      subscribe => File[$controller::file_mariadb],
      # Subscribe: Applique la ressource après la ressource cible. 
      # La ressources'actualise si la ressource cible change.
      #require   => Package[$controller::package_mariadb],
    }
  }
  else {
    package { $controller::package_mariadb :
      ensure => 'absent',
      notify => Exec['purge_mariadb'],
    }
      exec { 'purge_mariadb' :
        command     => '/bin/rm -Rf /var/lib/mysql',
        refreshonly => true,
      }
  }
}
