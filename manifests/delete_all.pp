#Classe DELETE ALL
class controller::delete_all{
  if $controller::manage_delete_all {
    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0740',
      owner   => '0',
      backup  => '.puppet-bak',
    }
    file { '/var/tmp/Delete_All_openstack.sh' :
      content => epp('controller/Delete_All_openstack.epp'),
    }
  }
}
