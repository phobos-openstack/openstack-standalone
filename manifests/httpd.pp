#Classe httpd
class controller::httpd {
  if $controller::manage_httpd {
    package { $controller::package_httpd :
      ensure => 'present',
      notify => File[$controller::file_httpd],
    }
    file { $controller::file_httpd :
      content => epp('controller/httpd.epp'),
      group   => 'root',
      mode    => '0644',
      owner   => 'root',
      require => Package[$controller::package_httpd],
      notify  => Service[$controller::service_httpd],
    }
    service { $controller::service_httpd :
      ensure    => 'running',
      enable    => true,
      subscribe => File[$controller::file_httpd],
    }
  }
  else {
    package { $controller::package_httpd :
      ensure => 'absent',
    }
    file { $controller::file_httpd :
      ensure => 'absent',
    }
  }
}
