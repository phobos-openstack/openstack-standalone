#Init Openstack Repos Network
class controller::install {

  if $controller::manage_install {
    package { $controller::package_repo  :
      ensure => 'present',
      notify => Exec['repo_upgrade'],
    }
      exec { 'repo_upgrade' :
      command     => $controller::upgrade_repo,
      timeout     => '500',
      refreshonly => true
      #Ne s'applique uniquement que lorsqu'une autre ressource le notifie.
      }

    package { $controller::package_openstack :
      ensure => 'present',
    }

    if $controller::manage_networkmanager {
      service { 'NetworkManager' :
        ensure => 'stopped',
        enable => false,
      }
    }

    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0644',
      owner   => '0',
    }
    file { '/etc/hosts' :
      content => epp('controller/host.epp'),
    }
    file { $controller::file_br_mgt :
      content => epp('controller/ifcfg-br-mgt.epp'),
      notify  => Service['network'],
    }
    file { $controller::file_br_ex :
      content => epp('controller/ifcfg-br-ex.epp'),
      notify  => Service['network'],
    }
    service { 'network' :
      ensure     => 'running',
      enable     => true,
      hasstatus  => true,
      hasrestart => true,
      subscribe  => File[$controller::file_br_mgt],
    }
  }
}
