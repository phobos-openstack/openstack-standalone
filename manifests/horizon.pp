#Classe horizon
class controller::horizon {
  if $controller::manage_horizon {

    package { $controller::package_horizon :
      ensure => 'present',
      notify => File[
        $controller::file_horizon,
        $controller::file_horizon_setting
      ],
    }

    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0640',
      owner   => 'root',
      backup  => '.puppet-bak',
      notify  => Exec['horizon_webservice'],
    }
      file { $controller::file_horizon :
        content => epp('controller/horizon.epp'),
      }
      file { $controller::file_horizon_setting :
        content => epp('controller/horizon_setting.epp'),
        group   => 'apache',
        mode    => '0644',
      }

      exec { 'horizon_webservice' :
        command     => $controller::horizon_webservice,
        refreshonly => true,
      }
  }
  else {
    package { $controller::package_horizon :
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_horizon : }
      file { $controller::file_horizon_setting : }
  }
}

