#Classe CINDER
class controller::cinder_backup {
  if $controller::manage_cinder_backup {

    #package { $controller::package_cinder_backup :
      #ensure => 'present',
      #notify => File[$controller::file_cinder_backup],
    #}
      #file { $controller::file_cinder_backup :
        #content => epp('controller/cinder_backup.epp'),
        #group   => '0',
        #mode    => '0644',
        #owner   => '0',
        #backup  => '.puppet-bak',
        #notify  => Service[$controller::service_cinder_backup],
      #}

        service { $controller::service_cinder_backup :
          ensure => 'running',
          enable => true,
        }
  }
  else {
    package { $controller::package_cinder_backup :
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_cinder_backup: }
  }
}
