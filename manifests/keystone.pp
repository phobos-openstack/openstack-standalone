#Classe keystone
class controller::keystone {
  if $controller::manage_keystone {
    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0644',
      owner   => '0',
      backup  => '.puppet-bak',
    }
      file { $controller::file_admin_openrc :
        content => epp('controller/admin-openrc.epp'),
      }
      file { '/var/tmp/install_keystone.sh' :
        content => epp('controller/install_keystone.epp'),
        mode    => '0744',
      }

    package { $controller::package_keystone :
      ensure => 'present',
      notify => Exec['config_keystone'],
    }
      exec { 'config_keystone' :
        command     => '/var/tmp/install_keystone.sh',
        require     => File[$controller::file_keystone],
        refreshonly => true,
      }
        file { $controller::file_keystone :
          content => epp('controller/keystone.epp'),
          group   => 'keystone',
          mode    => '0640',
          require => Package[$controller::package_keystone],
        }
  }
  else {
    package { $controller::package_keystone :
      ensure => 'absent',
    }
    File {
      ensure  => 'absent',
    }
      file { $controller::file_admin_openrc : }
      file { '/var/tmp/install_keystone.sh' : }
      file { $controller::file_keystone : }
  }
}
