#Classe SWIFT proxy
#installer et configurer le service proxy qui gère les demandes des services de compte, 
#de conteneur et d'objet opérant sur les nœuds de stockage.
#vous pouvez installer et configurer le service proxy sur plusieurs nœuds pour améliorer les performances et la redondance. 
class controller::swift_proxy {
  if $controller::manage_swift_proxy {
    File {
      ensure  => 'present',
      #group   => 'swift',
      #mode    => '0640',
      #owner   => '0',
      #backup  => '.puppet-bak',
    }
    package { $controller::package_swift_proxy :
      ensure => 'present',
      notify => File[$controller::file_swift_proxy],
    }
      file { $controller::file_swift_proxy :
        content => epp('controller/swift_proxy_server.epp'),
        require => Package[$controller::package_swift_proxy],
        notify  => Service[$controller::service_swift_proxy],
      }
    service { $controller::service_swift_proxy :
      ensure    => 'running',
      enable    => true,
      subscribe => File[$controller::file_swift_proxy],
    }
  }
  else {
    package { $controller::package_swift_proxy :
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_swift_proxy : }
  }
}
