#Classe SWIFT STORAGE
#installer et configurer des nœuds de stockage qui exploitent les services de compte, de conteneur et d'objet. 
class controller::swift_storage {
  if $controller::manage_swift_storage {
    File {
      ensure  => 'present',
      group   => 'swift',
      mode    => '0640',
      owner   => 'swift',
      backup  => '.puppet-bak',
    }
    file { '/var/tmp/install_swift_storage.sh' :
      content => epp('controller/install_swift_storage.epp'),
      mode    => '0744',
      group   => '0',
    }

    package { $controller::package_swift_storage :
      ensure => 'present',
      notify => Exec['config_swift'],
    }
      exec { 'config_swift' :
        command     => '/var/tmp/install_swift_storage.sh',
        require     => File[
          $controller::file_swift_storage_rsyncd,
          $controller::file_swift_storage_default_rsync,
          $controller::file_swift_storage_swift,
          $controller::file_swift_storage_account,
          $controller::file_swift_storage_container,
          $controller::file_swift_storage_object,
        ],
        refreshonly => true,
      }

        file { $controller::file_swift_storage_rsyncd  :
          content => epp('controller/swift_storage_rsyncd.epp'),
          require => Package[$controller::package_swift_storage],
          notify  => Service[$controller::service_swift_storage],
        }
        file { $controller::file_swift_storage_default_rsync  :
          content => epp('controller/swift_storage_default_rsync.epp'),
          require => Package[$controller::package_swift_storage],
          notify  => Service[$controller::service_swift_storage],
        }
        file { $controller::file_swift_storage_swift :
          content => epp('controller/swift_storage_swift.epp'),
          require => Package[$controller::package_swift_storage],
          notify  => Service[$controller::service_swift_storage],
        }
        file { $controller::file_swift_storage_account :
          content => epp('controller/swift_account_server.epp'),
          require => Package[$controller::package_swift_storage],
          notify  => Service[$controller::service_swift_storage],
        }
        file { $controller::file_swift_storage_container :
          content => epp('controller/swift_container_server.epp'),
          require => Package[$controller::package_swift_storage],
          notify  => Service[$controller::service_swift_storage],
        }
        file { $controller::file_swift_storage_object :
          content => epp('controller/swift_object_server.epp'),
          require => Package[$controller::package_swift_storage],
          notify  => Service[$controller::service_swift_storage],
        }

    service { $controller::service_swift_storage :
      ensure    => 'running',
      enable    => true,
      subscribe => File[
          $controller::file_swift_storage_rsyncd,
          $controller::file_swift_storage_default_rsync,
          $controller::file_swift_storage_swift,
          $controller::file_swift_storage_account,
          $controller::file_swift_storage_container,
          $controller::file_swift_storage_object,
      ],
      require   => Package[$controller::package_swift_storage],
    }
  }
  else {
    package { $controller::package_swift_storage :
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_swift_storage : }
  }
}
