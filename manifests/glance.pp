#Classe RabbitMQ
class controller::glance {

  if $controller::manage_glance {
    file { '/var/tmp/install_glance.sh' :
      content => epp('controller/install_glance.epp'),
      mode    => '0744',
      group   => '0',
    }
    package { $controller::package_glance :
      ensure => 'present',
      notify => Exec['config_glance'],
    }
      exec { 'config_glance' :
        command     => '/var/tmp/install_glance.sh',
        require     => File[
          $controller::file_glance_api,
          $controller::file_glance_registry
        ],
        refreshonly => true,
      }
        file { $controller::file_glance_api :
          content => epp('controller/glance-api.epp'),
          require => Package[$controller::package_glance],
          notify  => Service[$controller::service_glance],
        }
        file { $controller::file_glance_registry :
          content => epp('controller/glance-registry.epp'),
          require => Package[$controller::package_glance],
          notify  => Service[$controller::service_glance],
        }

    service { $controller::service_glance :
      ensure => 'running',
      enable => true,
      before => Exec['config_glance'],
    }
  }
  else {
    package { $controller::package_glance :
      ensure => 'absent',
    }
    File { $controller::file_glance :
      ensure => 'absent',
    }
    file { $controller::file_glance_api : }
    file { $controller::file_glance_registry : }
  }
}
