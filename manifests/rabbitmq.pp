#Classe RabbitMQ
class controller::rabbitmq {

  if $controller::manage_rabbitmq {
    file { '/var/tmp/install_rabbitmq.sh' :
      content => epp('controller/install_rabbitmq.epp'),
      group   => '0',
      mode    => '0744',
      owner   => '0',
      backup  => '.puppet-bak',
    }

    package { $controller::package_rabbitmq :
      ensure => 'present',
      before => File[$controller::file_rabbitmq],
      notify => Exec['config_rabbitmq'],
    }
      exec { 'config_rabbitmq' :
        command     => '/var/tmp/install_rabbitmq.sh',
        require     => Service[$controller::service_rabbitmq],
        refreshonly => true,
      }

    file { $controller::file_rabbitmq :
      content => epp('controller/rabbitmq.config.epp'),
      notify  => Service[$controller::service_rabbitmq],
      group   => 'rabbitmq',
      mode    => '0644',
      owner   => 'rabbitmq',
      backup  => '.puppet-bak',
    }

    service { $controller::service_rabbitmq :
      ensure    => 'running',
      enable    => true,
      before    => Exec['config_rabbitmq'],
      subscribe => File[$controller::file_rabbitmq],
    }
  }
  else {
    package { $controller::package_rabbitmq :
      ensure => 'absent',
    }
    file { $controller::file_rabbitmq :
      ensure => 'absent',
    }
  }
}
