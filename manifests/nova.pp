#Classe NOVA CONTROLLER
class controller::nova{

  if $controller::manage_nova {
    File {
      ensure  => 'present',
      group   => '0',
      mode    => '0644',
      owner   => '0',
      backup  => '.puppet-bak',
    }
    file { '/var/tmp/install_nova_controller.sh' :
      content => epp('controller/install_nova_controller.epp'),
      mode    => '0740',
    }

    package { $controller::package_nova_controller :
      ensure => 'present',
      notify => Exec['config_nova_controller'],
    }
      exec { 'config_nova_controller' :
        command     => '/var/tmp/install_nova_controller.sh',
        require     => File[$controller::file_nova_controller],
        notify      => Service[$controller::service_nova_controller],
        refreshonly => true,
      }
      file { $controller::file_nova_controller :
        content => epp('controller/nova.epp'),
        group   => 'nova',
        mode    => '0640',
        require => Package[$controller::package_nova_controller],
      }

    service { $controller::service_nova_controller :
      ensure    => 'running',
      enable    => true,
      subscribe => File[$controller::file_nova_controller],
      require   => Package[$controller::package_nova_controller],
    }
  }
  else {
    package { $controller::package_nova_controller :
      ensure => 'absent',
    }
    File{
      ensure => 'absent',
    }
      file { $controller::file_nova_controller : }
      file { '/var/tmp/install_nova_controller.sh' : }
  }
}




