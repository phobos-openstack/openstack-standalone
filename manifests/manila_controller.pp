#Classe manila_controller
class controller::manila_controller{
  if $controller::manage_manila_controller{
    File {
      ensure  => 'present',
      group   => 'manila',
      mode    => '0640',
      owner   => '0',
      #backup  => '.puppet-bak',
    }
      file { '/var/tmp/install_manila_controller.sh' :
        content => epp('controller/install_manila_controller.epp'),
        group   => '0',
        mode    => '0744',
      }

    package { $controller::package_manila_controller:
      ensure => 'present',
      notify => Exec['config_manila_controller'],
    }
      exec { 'config_manila_controller' :
        command     => '/var/tmp/install_manila_controller.sh',
        notify      => Service[$controller::service_mariadb],
        require     => File[$controller::file_manila_controller],
        refreshonly => true,
      }
        file { $controller::file_manila_controller:
          content => epp('controller/manila_controller.epp'),
          require => Package[$controller::package_swift_proxy],
        }

    service { $controller::service_manila_controller:
      ensure    => 'running',
      enable    => true,
      subscribe => File[$controller::file_manila_controller],
    }
  }
  else {
    package { $controller::package_manila_controller:
      ensure => 'absent',
    }
    File {
      ensure => 'absent',
    }
      file { $controller::file_manila_controller: }
  }
}
